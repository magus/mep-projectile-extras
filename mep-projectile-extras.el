;;; mep-projectile-extras.el --- Some extra stuff for projectile -*- lexical-binding: t -*-

;; Author: Magnus Therning
;; Maintainer: Magnus Therning
;; Version: 0
;; Package-Requires: ((projectile) (xref))
;; Homepage: none
;; Keywords: projectile


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Just some useful functions, really.

;;; Code:

(require 'f)
(require 'org)
(require 'projectile)
(require 'xref)



(defvar mep-projectile-extras--store (make-hash-table :test 'equal)
  "The store of project parameters.")

(defun mep-projectile-extras-get-parameter (param)
  "Return project parameter PARAM, or nil if unset."
  (let ((key (cons (projectile-project-name) param)))
    (gethash key mep-projectile-extras--store nil)))

(defun mep-projectile-extras-set-parameter (param value)
  "Set the project parameter PARAM to VALUE."
  (let ((key (cons (projectile-project-name) param)))
    (puthash key value mep-projectile-extras--store))
  value)

;;;###autoload
(defun mep-projectile-extras-kill-project-store (&optional proj-name)
  "Remove all parameters associated with the current project from the store.

If PROJ-NAME isn't provided the current project's name is used."
  (let* ((proj-name (or proj-name (projectile-project-name)))
         (filtered-keys (seq-filter (lambda (key) (equal (car key) proj-name))
                                    (hash-table-keys mep-projectile-extras--store))))
    (dolist (key filtered-keys)
      (remhash key mep-projectile-extras--store))))

;;;###autoload
(defun mep-projectile-extras-xref-history (&optional new-value)
  "Return project-local xref history for the current projectile.

Override existing value with NEW-VALUE if it's set."
  (if new-value
      (mep-projectile-extras-set-parameter 'xref--history new-value)
    (or (mep-projectile-extras-get-parameter 'xref--history)
        (mep-projectile-extras-set-parameter 'xref--history (xref--make-xref-history)))))



;;;###autoload
(defun mep-projectile-async-shell-command ()
  "Run `async-shell-command' in the current project's root directory."
  (declare (interactive-only async-shell-command))
  (interactive)
  (let ((default-directory (projectile-project-root)))
    (call-interactively #'async-shell-command)))



;;;###autoload
(defun mep-projectile-open-todo ()
  "Open the project's todo file."
  (interactive)
  (if-let* ((proj-dir (projectile-project-root))
            (proj-todo-file (f-join proj-dir "TODO.org")))
      (org-open-file proj-todo-file)
    (message "Not in a project")))

(provide 'mep-projectile-extras)
;;; mep-projectile-extras.el ends here
